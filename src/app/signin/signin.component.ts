import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from './signin.service';
import { AppService } from '../shared/app.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.sass']
})
export class SigninComponent implements OnInit {
  constructor(private authService: AuthenticationService , private appService: AppService) { }

  ngOnInit(): void {
    this.login()
  }

  login = (): void => {
    const loginDetails =
   /* {
      userName: 'sureshkumarn.dev@auxolabs.dev',
      password: 'Next@123'
    }

      {
      userName: 'sureshkumar.n@dev.concord.net',
      password: 'UserDEV@123'
    }*/
    {
      userName: 'sureshkumar.n@qa.concord.net',
      password: 'UserDev@123'
    }

    this.authService.loginUser(loginDetails).subscribe(response => {
      this.setLoginData(response);
    }, error => {
      console.log(error.error)
    });
  }

  setLoginData = (data: object): void => {
    localStorage.setItem('userinfo', JSON.stringify(data));
    localStorage.setItem('isAuthenticated', 'Yes');
    this.appService.navigateTo('/dashboard');
  }
}
