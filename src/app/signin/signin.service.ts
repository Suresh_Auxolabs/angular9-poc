import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment'
import { Constants } from 'src/constants';
import { AppService } from '../shared/app.service';

@Injectable()
export class AuthenticationService {

  constructor(private appService: AppService) { }

  loginUser(userData: any) {
    return this.appService.
      postAction(environment.apiUrl + Constants.endpointUrls.login, userData);
  }

  viewingSession(userData: any) {
    return this.appService.
      postAction(environment.apiUrl + Constants.endpointUrls.login, userData);
  }

  medetails() {
    return this.appService.
      getAction(environment.apiUrl + Constants.endpointUrls.me);
  }

  getsettingData() {
    return this.appService.
      getAction(environment.apiUrl + Constants.endpointUrls.queues+'5c76a7435b53411a149de3d3/status');
  }

}
