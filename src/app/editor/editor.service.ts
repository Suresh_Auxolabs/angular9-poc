import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment'
import { Constants } from 'src/constants';
import { AppService } from '../shared/app.service';

@Injectable()
export class EditorService {

  constructor(private appService: AppService) { }

  viewingSession(url) {
    return this.appService.
    postAction(environment.apiUrl + url);
  }

}
