import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import * as $ from 'jquery';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import * as _ from 'lodash';
import { templateContentData } from './templateContent';
import { HttpRequest, HttpHeaders, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { SharedService } from '../shared/shared.service'
import { EditorService } from './editor.service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    Authorization: 'my-auth-token'
  })
};
declare var window: any;
declare var PCCViewer: any;

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.sass']
})
export class EditorComponent implements OnInit {
  @ViewChild('docViewer') docViewer: ElementRef;
  title = 'prizmDocPoc';
  enableSearchablePDF: any
  backupViewingSessionId: any
  tesdata :any
  viewSessionId: any
  docmentId: '5bd16e01a187d52b348719af';
  templateContent = templateContentData;
  imageHandlerUrl = 'https://nextstep-dev.concord.net/app/viewer-webtier/pas.php/'
  viewerAssetsPath: 'https://nextstep-dev.concord.net/app/viewer-assets/img'
  htmlEliment: any
  constructor(private http: HttpClient, private sharedService: SharedService, private editorService: EditorService) { }

  ngOnInit(): void {
    this.createSession()

  }

 

  createSession = (): void => {
    // const orgUrl = 'viewer/sessions?queueId=5f2d0174e6b91170442419cf&documentId=5f2d0196d7887000012ac9d7&partitionKey=5f28fbdd9289d8599034acd5-4&version=0&isRefresh=false' //DEV
       const orgUrl = 'viewer/sessions?queueId=5f29179f0f2f910f64ffc5fe&documentId=5f291d94b098260001772e8b&partitionKey=5ee8b8c340923f2bf4a87ba4-8&version=0&isRefresh=false' // QA
    this.editorService.viewingSession(orgUrl).subscribe(response => {
      this.viewSessionId = response.viewingSessionId;
      PCCViewer.Ajax.headers = {
        'Accusoft-Affinity-Token': null,
        Authorization: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwczovL25leHRzdGVwLmNvbmNvcmQubmV0L3VzZXItaWQiOiI1YzYxMTgxNzkxYWNjODE4MjA0NjY3YzgiLCJodHRwczovL25leHRzdGVwLmNvbmNvcmQubmV0L3VzZXItYWNjb3VudC1udW1iZXIiOiI5MDcwMSIsImh0dHBzOi8vbmV4dHN0ZXAuY29uY29yZC5uZXQvYWNjb3VudC1pZCI6IjVjNjExODExOTFhY2M4MTgyMDQ2NjdiYiIsImh0dHBzOi8vbmV4dHN0ZXAuY29uY29yZC5uZXQvYWNjb3VudC1udW1iZXIiOiI2MzIzOSIsImh0dHBzOi8vbmV4dHN0ZXAuY29uY29yZC5uZXQvYWNjb3VudC10eXBlIjoiVXNlckFjY291bnQiLCJodHRwczovL25leHRzdGVwLmNvbmNvcmQubmV0L3VzZXItbmFtZSI6InN1cmVzaGt1bWFybi5kZXZAYXV4b2xhYnMuaW4iLCJodHRwczovL25leHRzdGVwLmNvbmNvcmQubmV0L3Bhc3N3b3JkIjoiVGVzdEAxMjMiLCJodHRwczovL25leHRzdGVwLmNvbmNvcmQubmV0L2VtYWlsIjoic3VyZXNoa3VtYXJuLmRldkBhdXhvbGFicy5pbiIsImh0dHBzOi8vbmV4dHN0ZXAuY29uY29yZC5uZXQvZmlyc3QtbmFtZSI6IlN1cmVzaGt1bWFyICBOIiwiaHR0cHM6Ly9uZXh0c3RlcC5jb25jb3JkLm5ldC90aW1lem9uZSI6IihHTVQtMDg6MDApIFBhY2lmaWMgVGltZSAoVVMgJiBDYW5hZGEpIiwiaHR0cHM6Ly9uZXh0c3RlcC5jb25jb3JkLm5ldC9waG9uZS1udW1iZXIiOiIiLCJodHRwczovL25leHRzdGVwLmNvbmNvcmQubmV0L2ZheC1udW1iZXIiOiIxMjE0MTExNDE2MyIsImh0dHBzOi8vbmV4dHN0ZXAuY29uY29yZC5uZXQvT3duZXJDdXN0b21lcklkIjoiMCIsImh0dHBzOi8vbmV4dHN0ZXAuY29uY29yZC5uZXQvT3duZXJUeXBlIjoiIiwiaHR0cHM6Ly9uZXh0c3RlcC5jb25jb3JkLm5ldC9NYnhVc2VyTmFtZSI6Im1ieDA5OTAwNjU4IiwiaHR0cHM6Ly9uZXh0c3RlcC5jb25jb3JkLm5ldC91c2VyLWFwaUtleSI6IjRlNmQ5ODBhLTE0MGYtNDA0Ny1iNmRkLWEyN2Y3YWZlYTZmMiIsImh0dHBzOi8vbmV4dHN0ZXAuY29uY29yZC5uZXQvRGVmYXVsdENvdmVyUGFnZSI6IkNvbmNvcmQgRGVmYXVsdCIsImh0dHBzOi8vbmV4dHN0ZXAuY29uY29yZC5uZXQvQ29tcGFueU5hbWUiOiJBdXhvIExhYnMgU3RhZ2VIdWIiLCJodHRwczovL25leHRzdGVwLmNvbmNvcmQubmV0L0xhc3RMb2dpbiI6IjA1LzE4LzIwMjAgMDM6NTg6MjEiLCJodHRwczovL25leHRzdGVwLmNvbmNvcmQubmV0L0lzQXp1cmVTZWFyY2hMaWNlbnNlZCI6IlRydWUiLCJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3JvbGUiOiJVc2VyIiwibmJmIjoxNTg5Nzc1MDE2LCJleHAiOjE1ODk4MDM4MTYsImlzcyI6Im5leHRzdGVwLXFhLmNvbmNvcmQubmV0IiwiYXVkIjoibmV4dHN0ZXAtcWEuY29uY29yZC5uZXQifQ.Ca7TDoOEHOmJKnQnM7yvCOSXXD1-I34W5gGe_yByXJA'
      };
      this.getResourcesAndEmbedViewer({
        options: {
          imageHandlerUrl: this.imageHandlerUrl,
          resourcePath: this.viewerAssetsPath
        }
      });
    }, error => {
      console.log(error.error)
    });
  }

  getResource(url, dataType) {
    return $.ajax({
      url,
      dataType
    })
      .then(function (response) {
        return response;
      });
  }

  getJson(fileName) {
    return this.getResource(fileName, '')
      .then(function (response) {
        if (typeof response === 'string') {
          return JSON.parse(response);
        }
        return response;
      });
  }

  getResourcesAndEmbedViewer(demoConfig) {
    const sessionData = {};
    $.when(
      this.docmentId,
      this.viewSessionId,
      this.templateContent,
      this.getJson('assets/viewer-assets/languages/en-US.json'),
      this.getResource('assets/viewer-assets/icons/svg-icons.svg', 'text'),
      demoConfig.options || {})
      .done(this.buildViewerOptions);
  }


  buildViewerOptions() {
    const args = [].slice.call(arguments);
    const optionsOverride = args.pop();
    const options = {
      viewMode: 'Document',
      annotationsMode: 'LayeredAnnotations',
      documentID: encodeURIComponent(args[1]),
      icons: args[4],
      language: args[3],
      template: args[2],
      signatureCategories: 'Signature,Initials,Title'
    };
    const combinedOptions = _.extend(optionsOverride, options);
    setTimeout(() => {
      const viewer = window.pccViewer($('#docViewer'), combinedOptions);
    }, 3000);
  }

  ngAfterViewInit(): void {
    this.htmlEliment = this.docViewer.nativeElement
  }
}



