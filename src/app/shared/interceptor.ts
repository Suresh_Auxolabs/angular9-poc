import { Component, OnInit, Inject } from '@angular/core';
import { Injectable, } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { ReflectiveInjector } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { Constants } from 'src/constants';
import { AppInsightService } from './app-insight';
import { AlertDialogComponent ,AlertTypes } from './components';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { AppInsights } from "applicationinsights-js";

@Injectable()

export class ErrorInterceptor implements HttpInterceptor {
  private appInsightService: AppInsightService;
  constructor(private dialog: MatDialog) {
    const injector = ReflectiveInjector.resolveAndCreate([
      AppInsightService
    ]);
    this.appInsightService = injector.get(AppInsightService);
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    request = request.clone({
      withCredentials: true
    });
    return next.handle(request).pipe(
      catchError(error => {
        const requestTimeMS = performance.now();
        const userInfomations: any = JSON.parse(localStorage.getItem('userinfo'))
        const stackTrace: any = {
          name: userInfomations.userName,
          type: 'API-Response-Error-POC',
          method: 'GET',
          url: request.url,
          duration: (performance.now() - requestTimeMS).toFixed(2) + ' ms',
          status: error.status,
          status_description: error.statusText || 'No status description provided',
          error_message: error.error ? JSON.stringify(error.error.message) : 'No error response was provided',
          email_address: userInfomations ? userInfomations.emailAddress : 'No user information provided',
          customer_number: userInfomations ? userInfomations.customerNumber : 'No user information provided'
        };

        console.log(stackTrace)
        // this.appInsightService.logError(JSON.stringify(stackTrace) )
        AppInsights.trackException(stackTrace.type, stackTrace.error_response, stackTrace);

        let message = ''
        if (error instanceof HttpErrorResponse) {
          if (error instanceof ErrorEvent) {
            console.error('Error Event');
          } else {
            message = error.error && error.error.message
            switch (error.status) {
              case 401:      // login
                message = message ? message : Constants.messages[error.status]
                break;
              case 403:     // forbidden
                message = message ? message : Constants.messages[error.status]
                break;
              case 400:     // forbidden
                message = message ? message : Constants.messages[error.status]
                break;
             case 404:     // forbidden
                message = message ? message : Constants.messages[error.status]
                break;
            default:     // forbidden
                message = message ? message : Constants.messages.default
                break;
            }
            const dialogRef = this.dialog.open(AlertDialogComponent, {
              panelClass: 'alert-dialog',
              disableClose: true,
              data: {
                msg: message,
                type: AlertTypes.ALERTCONFIRM
              }
            });
            dialogRef.keydownEvents().subscribe(e => {
              if (e.keyCode === 27) {
                dialogRef.close();
              }
            });
            dialogRef.afterClosed().subscribe(result => {
              if (result) {

              }
            });

          }
        } else {
          console.error('Other Errors');
        }
        return throwError(error);
      })
    );
  }

    handleError(error) {
    this.appInsightService.logError(error);
  }
}
