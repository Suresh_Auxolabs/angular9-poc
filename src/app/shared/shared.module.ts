import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AlertDialogComponent } from '../shared/components/index';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import { MatModule } from './mat.module';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {ErrorInterceptor} from './interceptor';
import { NgxSpinnerModule } from "ngx-spinner";
import { SharedService } from './shared.service';


@NgModule({
  declarations: [
    AlertDialogComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatModule,
    BrowserModule,
    BrowserAnimationsModule,
    NgxSpinnerModule
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    AlertDialogComponent,
  ],
  entryComponents: [
    AlertDialogComponent
  ],
  providers: [
    SharedService, {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true}
  ]
})
export class SharedModule { }
