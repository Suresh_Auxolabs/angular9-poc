import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../signin/signin.service';
import { AppService } from '../shared/app.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass']
})
export class DashboardComponent implements OnInit {

  constructor(private authService: AuthenticationService, private appService: AppService, ) { }

  ngOnInit(): void {
    this.getMe()
  }


  getMe = (): void => {
    this.authService.medetails().subscribe(response => {
      this.setLoginData(response)
      this.appService.navigateTo('/editor')
     // this.getSettings()
    }, error => {
      console.log(error.error)
    });
  }

  getSettings = (): void => {
    this.authService.getsettingData().subscribe(response => {
      console.log(response)
    }, error => {
      console.log(error.error)
    });
  }


  setLoginData = (data: object): void => {
    // console.log('ddddd',customdata)
    localStorage.setItem('medatails', JSON.stringify(data));
    localStorage.setItem('isAuthenticated', 'Yes');
  }

}
