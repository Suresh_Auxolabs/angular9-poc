import { BrowserModule } from '@angular/platform-browser';
import { NgModule,ErrorHandler } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { LocationStrategy, HashLocationStrategy} from '@angular/common';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { SigninComponent } from './signin/signin.component';
import { AuthenticationService } from './signin/signin.service';
import { AppService } from './shared/app.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {ErrorInterceptor} from './shared/interceptor';
import { DashboardComponent } from './dashboard/dashboard.component'
import { AuthGuard } from './shared/authGuard.service';
import {AppInsightService} from './shared/app-insight';
import {CustomErrorHandler} from './shared/custom-error-handler';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxSpinnerModule } from 'ngx-spinner';
import { EditorComponent } from './editor/editor.component';
import { EditorService } from './editor/editor.service';


@NgModule({
  declarations: [
    AppComponent,
    SigninComponent,
    DashboardComponent,
    EditorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NgxSpinnerModule

  ],
  providers: [AuthenticationService, AppInsightService, AppService, EditorService,
    AuthGuard, {provide: LocationStrategy, useClass: HashLocationStrategy},
    {
      provide: ErrorHandler,
      useClass: CustomErrorHandler,
       },],
  bootstrap: [AppComponent]
})
export class AppModule { }
