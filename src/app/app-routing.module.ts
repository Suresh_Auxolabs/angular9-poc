import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SigninComponent } from './signin/signin.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthGuard } from './shared/authGuard.service';
import { EditorComponent } from './editor/editor.component';

const routes: Routes = [
{ path: '', redirectTo: 'login', pathMatch: 'full' },
{ path: 'login', component: SigninComponent },
{ path: 'dashboard', component: DashboardComponent , canActivate: [AuthGuard] },
{ path: 'editor', component: EditorComponent , canActivate: [AuthGuard] },
{ path: '**', redirectTo: 'login' }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
