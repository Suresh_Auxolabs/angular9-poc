import { Component } from '@angular/core';
import { AppInsightService } from './shared/app-insight';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'login';

  constructor(
    private appInsightService: AppInsightService,
  ) {}
}
